# gwt-webworker
WebWorker classes from Googles SpeedTracer extracted as own package to integrate in own projects.

Maven integraten
----------------

The dependency itself for GWT-Projects:

```
    <dependency>
      <groupId>de.knightsoft-net</groupId>
      <artifactId>gwt-webworker</artifactId>
      <version>1.0.10</version>
    </dependency>
```

GWT Integration
---------------

The project using web workers mustn't compile different permutations, because inside
a web worker the browser or language version can't be detected. So separate the web worker
from the rest of your application. You also have to take care, window is not accessed in
the java-script code

What you now have to insert into your .gwt.xml file is:

```
<inherits name='com.google.gwt.webworker.WebWorker' />

<set-property name="user.agent" value="safari" />
<set-configuration-property name="user.agent.runtimeWarning" value="false" />

<!-- disable logging, otherwise window is involved -->
<set-property name="gwt.logging.enabled" value="FALSE"/>
<set-property name="gwt.logging.consoleHandler" value="DISABLED"/>

<!-- Use the WebWorker linker for a Dedicated worker -->
<add-linker name="dedicatedworker" />
```

Now you can implement your Web Worker class which has to extend DedicatedWorkerEntryPoint
and implement MessageHandler. With postMessage(String) the Web Worker can send Messages to
the calling application and the onMessage(MessageEvent) method receives messages sent by the
application. In `onWorerkload` you set the worker as message handler.

```
public class TestWorker extends DedicatedWorkerEntryPoint implements MessageHandler {

  @Override
  public final void onMessage(final MessageEvent event) {
    final UncaughtExceptionHandler uncoughtExHand = GWT.getUncaughtExceptionHandler();
    if (event != null && event.getDataAsString() != null) {
      final String dataAsString = event.getDataAsString());
      try {
        // do something with the data
        ...
        // you can send result to the application
        this.postMessage("Some result for the application");
      } catch (final Exception e) {
        uncoughtExHand.onUncaughtException(e);
        return;
      }
    }
  }

  @Override
  public final void onWorkerLoad() {
    this.setOnMessage(this);
  }
}

```

On application side you have to create one Error Handler and Message Handler to receive 
Messages from the worker. Now you can create a Web Worker thread and post messages to him.
If you create more workers at the same time, you can use multithreading and take profit of
Multicore CPUs even in the browser.

```
final ErrorHandler webWorkerErrorHandler = new ErrorHandler() {
  @Override
  public void onError(final ErrorEvent pEvent) {
    // handle error
  }
}

final MessageHandler webWorkerMessageHandler = new MessageHandler() {
  @Override
  public final void onMessage(final MessageEvent pEvent) {
    // handle message
  }
}
..

Worker worker = Worker.create(<javascriptname>)
worker.setOnMessage(webWorkerMessageHandler);
worker.setOnError(webWorkerErrorHandler);

worker.postMessage("(may be serialized) data to handle in web worker");
```
That's all.

Examples how to use, you can find in the [SimpleGWTWebWorker](https://github.com/RichardMorris/SimpleGWTWebWorker)
project from Richard Morris, or in my [Base16KTest](https://www.knightsoft-net.de/base16k/) Project
([Sources](https://www.knightsoft-net.de/Base16KTest.tar.bz2)).